extends Control

func _ready():
	Globals.deathscreen = $"/root/Game/CanvasLayer/Death Screen"
	modulate = Color.TRANSPARENT
	pass

func flash():
	var tween = get_tree().create_tween()
	modulate = Color.WHITE
	tween.tween_property(self, "modulate", Color.TRANSPARENT, 0.4)
	
