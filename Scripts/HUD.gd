extends Control

@onready var deathstext = $MarginContainer/Label
@onready var timetext = $MarginContainer2/Time

func _process(_delta):
	if Globals.current_course == "none":
		visible = false
	else:
		visible = true
	deathstext.text = "Deaths: " + str(Globals.deaths)
	if Globals.time:
		timetext.text = str(Globals.timestring)

