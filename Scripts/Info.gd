extends Control

@onready var titley = $MarginContainer/VBoxContainer/Title
@onready var subtitley = $MarginContainer/VBoxContainer/Subtitle

func _ready():
	Globals.info = $/root/Game/CanvasLayer/Info
	modulate = Color.TRANSPARENT

func showtitle(title, subtitle=""):
	var tween = get_tree().create_tween()
	titley.text = title
	subtitley.text = subtitle
	tween.tween_property(self, "modulate", Color.WHITE, 0.2)
	
func hidetitle():
	var tween = get_tree().create_tween()
	tween.tween_property(self, "modulate", Color.TRANSPARENT, 0.2)
