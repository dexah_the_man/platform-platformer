extends Control

var on = false

@onready var sensitivity = $PanelContainer/VBoxContainer/SensSlider
@onready var fov = $PanelContainer/VBoxContainer/FovSlider

func _ready():
	sensitivity.value = 2
	fov.value = 75
	
func _process(_delta):
	Globals.sensitivity = sensitivity.value * 0.001
	Globals.fov = fov.value
	
	visible = false
	if on:
		visible = true
		
	if Input.is_action_just_pressed("ui_cancel") and Globals.winner == false:
		on = !on
		get_tree().paused = on
		if on == true:
			Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
		if on == false:
			Input.mouse_mode = Input.MOUSE_MODE_CAPTURED


func _on_button_pressed():
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
	on = false
	print("ey")
