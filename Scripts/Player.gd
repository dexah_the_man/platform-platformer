extends CharacterBody3D

var gravity = ProjectSettings.get_setting("physics/3d/default_gravity")
var speed = 10.0
var jump_speed = 5
var mouse_sensitivity = 0.002
var jumps = 0
var spawnpoint_type = 2
# 0 = Regular, 1 = Spawn, 2 = HubSpawn
var on_tele = false
var just_tele = false
var dash = false
var acceleration = 0.9
var dashfov:float
var dashboost = 1.0
var moving = false
var wall_normal
var wallruning = false
var walljumped = false

@onready var head = $Head
@onready var camera = $Head/Camera3D
@onready var hub_spawn = $"/root/Game/World/Spawnpoint"
@onready var spawnpoint = hub_spawn.position
@onready var animatetree = $AnimationTree
func _ready():
	position = spawnpoint
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED

func wallrun():
	wallruning = false
	if Input.is_action_pressed("jump"):
		if Input.is_action_pressed("forward"):
			if is_on_wall() and !walljumped:
				wall_normal = get_wall_normal()
				velocity.y = 0 
				wallruning = true
				

func _physics_process(delta):
	
	
	dashfov = Globals.fov * 2.0
	## teleport stuf
	if on_tele:
		if Input.is_action_just_pressed("use"):
			global_transform.origin = on_tele.get_node(on_tele.link).global_transform.origin
			global_transform.origin.y += 2
			just_tele = true
			
	## Setting settings
	mouse_sensitivity = Globals.sensitivity
	
	
	## Restarting logic
	if Input.is_action_just_pressed("restart"):
		if spawnpoint_type == 0:
			die()
		elif spawnpoint_type == 1 or spawnpoint_type == 2:
			reset()

	## Die stuff too
	if global_transform.origin.y < -30:
		die()
	
	## Movement logic
	velocity.y += -gravity * delta
	var input = Input.get_vector("left", "right", "forward", "back")
	var movement_dir = transform.basis * Vector3(input.x, 0, input.y)
	if abs(movement_dir.x) > 0 or abs(movement_dir.z) > 0.5:
		moving = true
	else: 
		moving = false
	if Input.is_action_just_pressed("sprint") and !dash and !is_on_floor() and moving:
		velocity.x = movement_dir.x * (speed*200) * delta 
		velocity.z = movement_dir.z * (speed*200) * delta
		camera.fov = Globals.fov * 1.3
		dash = true  
	velocity.x = lerp(velocity.x, movement_dir.x * speed, acceleration * delta)
	velocity.z = lerp(velocity.z, movement_dir.z * speed, acceleration * delta)
	if Input.is_action_just_released("jump") and wallruning and !walljumped:
		walljumped = true
		velocity += wall_normal*10 + Vector3(0,10,0)
		print("ahh!")
	wallrun()
	print(wallruning)
	move_and_slide()
	
	if !is_on_floor():
		acceleration = 5
		camera.fov = lerp(camera.fov, Globals.fov, 0.2*delta)
	
	if is_on_floor():
		walljumped = false
		camera.fov = lerp(camera.fov, Globals.fov, 3*delta)
		acceleration = 20
		jumps = 2
		dash = false
		if moving == true:
			head.position.y = lerp(head.position.y, abs(sin(Time.get_ticks_msec()*0.01)*35)*delta+0.5, 5*delta)
			head.position.x = lerp(head.position.x, sin(Time.get_ticks_msec()*0.01)*10*delta - (sin(Time.get_ticks_msec()*0.01)*10*delta+0.5)/2, 5*delta)
		else:
			head.position.y = lerp(head.position.y, 0.682, 10*delta)
			head.position.x = lerp(head.position.x, 0.0, 10*delta)
			
		
	if jumps >= 1 and Input.is_action_just_pressed("jump"):
		velocity.y = jump_speed
		print(spawnpoint)
		jumps -= 1
		

		
	animatetree.set("parameters/conditions/idle", Input.get_vector("left", "right", "forward", "back") == Vector2(0,0))
	animatetree.set("parameters/conditions/run", Input.get_vector("left", "right", "forward", "back") != Vector2(0,0))
		
		
func die():
	print("oweire")	
	position = spawnpoint
	
	Globals.deaths += 1
	Globals.deathscreen.flash()
	
	
func reset():
	position = spawnpoint
	
	
func _input(event):
	if event is InputEventMouseMotion and Input.mouse_mode == Input.MOUSE_MODE_CAPTURED:
		rotate_y(-event.relative.x * mouse_sensitivity)
		head.rotate_x(-event.relative.y * mouse_sensitivity)
		head.rotation.x = clampf(head.rotation.x, -deg_to_rad(70), deg_to_rad(70))
		
	if event is InputEventMouseButton:
		if event.button_index == 1:
			Input.mouse_mode = Input.MOUSE_MODE_CAPTURED


func _on_area_3d_area_entered(area):
	var body = area.get_owner()
	
	if body.is_in_group("checkpoint"):
		if body.mode == 0:
			if Globals.current_course == body.course_name:
				spawnpoint = body.position
				spawnpoint_type = 0
				spawnpoint.y += 2
				Globals.popups.popup("Got checkpoint!")
		if body.mode == 1:
			spawnpoint = body.position
			spawnpoint_type = 1
			spawnpoint.y += 2
			Globals.current_course = body.course_name
			Globals.start_time = Time.get_ticks_msec()
			Globals.deaths = 0
			Globals.popups.popup("Started course " + Globals.current_course + "!")
		if body.mode == 2:
			if Globals.current_course == body.course_name:
				print("Congrats! You beat " + body.course_name + " in blank.")
				Globals.winner = true
				spawnpoint = hub_spawn.position
				spawnpoint_type = 2
				
				
	if body.is_in_group("telepad"):
		on_tele = body
		Globals.info.showtitle("E to Teleport")
		print(on_tele)
			
			
		
	if area.is_in_group("HubArea"):
		if Globals.current_course != "none":
			Globals.popups.popup("Entering hub, exiting course " + Globals.current_course + ".")
			spawnpoint = hub_spawn.position
			spawnpoint_type = 2
			Globals.current_course = "none"


func _on_area_3d_area_exited(area):
	var body = area.get_owner()
	
	if body.is_in_group("telepad"):
		Globals.info.hidetitle()
		on_tele = null
		if on_tele:
			just_tele = false


func _on_area_3d_body_entered(body):
	print("ahh")
	if body.is_in_group("kill"):
		print("OW!")
		die()
