extends Control

func _ready():
	modulate = Color.TRANSPARENT
	Globals.popups = $/root/Game/CanvasLayer/Popups/Popup
	
func popup(text):
	$MarginContainer/PanelContainer/Label.text = text
	var tween = get_tree().create_tween()
	tween.tween_property(self, "modulate", Color.WHITE, 0.2)
	tween.tween_property(self, "modulate", Color.WHITE, 1)
	tween.tween_property(self, "modulate", Color.TRANSPARENT, 0.2)
