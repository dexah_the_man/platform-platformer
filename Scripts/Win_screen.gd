extends Control

@onready var coursename = $PanelContainer/VBoxContainer/coursename
@onready var time = $PanelContainer/VBoxContainer/time
@onready var player = $/root/Game/Player

var on = false

func _ready():
	visible = false

func _process(_delta):
	if Globals.winner == true:
		coursename.text = Globals.current_course
		time.text = "in " + str(Globals.timestring) + "\nwith " + str(Globals.deaths) + " deaths!"
		get_tree().paused = true
		visible = true
		if Input.is_action_just_pressed("jump"):
			Globals.current_course = "none"
			visible = false
			Globals.winner = false
			get_tree().paused = false
			player.reset()
