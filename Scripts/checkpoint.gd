extends Node3D

enum MODE { regular,start,finish }
@export var mode: MODE
@export var course_name: String

func _ready():
	add_to_group("checkpoint")
