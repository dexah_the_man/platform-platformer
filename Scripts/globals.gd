extends Node

var sensitivity = 0.002
var fov = 90.0
var current_course : String = "none"
var winner = false
var deaths = 0
var start_time
var current_time
var time
var timestring

# Nodes for UI elements, easy access!
var popups : Node
var info : Node
var deathscreen : Node


static func format(timey: float) -> String:
	return "%01d:%02d.%02d" % [timey / 60, fmod(timey, 60), fmod(timey,1) * 100]


func display_time(msec_time : int) -> String:
	var leftover_msec = msec_time % 1000
	var sec = msec_time / 1000
	var mins = sec / 60
	sec %= 60
	return "%02d:%02d.%02d" % [mins, sec, leftover_msec]

func _ready():
	print(popups)

func _process(_delta):
	if current_course != "none":
		current_time = Time.get_ticks_msec()
		time = current_time - start_time
		timestring = display_time(time)
