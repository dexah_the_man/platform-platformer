extends Control

@onready var animator = $AnimationPlayer
@onready var transitioner = $transition

func _ready():
	animator.play("Type_in")
	await animator.animation_finished
	await get_tree().create_timer(1).timeout
	transitioner.transition_to("res://Scenes/game.tscn")
