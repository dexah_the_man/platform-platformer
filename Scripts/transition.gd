extends ColorRect

# Path to the next scene to transition to
@export var next_scene_path : String

# Reference to the _AnimationPlayer_ node
@onready var _anim_player := $Animations

func _ready() -> void:
	# Plays the animation backward to fade 
	_anim_player.play("Fade_in")
	
func transition_to(_next_scene := next_scene_path) -> void:
	next_scene_path = _next_scene
	# Plays the Fade animation and wait until it finishes
	_anim_player.play("Fade_out")

func _on_animation_player_animation_finished(anim_name):
	if anim_name == "Fade_out":
		get_tree().change_scene_to_file(next_scene_path)
